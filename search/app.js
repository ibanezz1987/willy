import Search from './index.vue';

if (document.querySelector('#vue-search')) {
    new Vue({
        el: '#vue-search',
        template: '<Search />',
        components: { Search },
    });
}
