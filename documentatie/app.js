import VueRouter from 'vue-router';
import store from './store';
import Main from './main.vue';

import Detail from './components/detail.vue';

Vue.use(VueRouter);

const elem = document.querySelector('#vue-documentatie');

if (elem) {
    const pageUrl = elem.dataset.url;

    const routes = [{ path: '*', component: Detail }];

    const router = new VueRouter({
        routes,
        mode: 'history',
        base: pageUrl,
    });

    new Vue({
        el: '#vue-documentatie',
        store,
        template: '<Main />',
        components: { Main },
        router,
    }).$mount('#app');
}
