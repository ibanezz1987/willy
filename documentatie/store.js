import axios from 'axios';
import { URL } from './../globals';

Vue.use(Vuex);

/**
 * Goed voorbeeld: https://github.com/vuejs/vuex/tree/dev/examples/shopping-cart/store
 */
export default new Vuex.Store({
    /**
     * State
     * https://vuex.vuejs.org/en/state.html
     */
    state: {
        /**
         * Documentatie detail
         */
        documentationEntry: null,

        /**
         * Documentatie structure
         */
        documentationStructure: {},
    },

    /**
     * Actions
     * Vergelijkbaar met mutations, alleen vanuit hier kun je geen state aanpassen, maar een mutation doen
     * https://vuex.vuejs.org/en/actions.html
     */
    actions: {
        /**
         * Documentatie entry ophalen
         *
         * @param {*} { commit }
         * @param {string} { uri }
         */
        fetchDocumentationEntry({ commit }, uri) {
            return axios({
                url: `${URL.api}/documentation?uri=${uri}`,
                method: 'get',
            })
                .then(result => {
                    if (result.status === 200) {
                        commit('setDocumentationEntry', result.data.data[0]);
                    }
                })
                .catch(error => {
                    console.error(error);
                });
        },

        /**
         * Documentatie structuur
         *
         * @param {*} { commit }
         * @param {string} { uri }
         */
        fetchDocumentationStructure({ commit }, uri) {
            return axios({
                url: `${URL.api}/documentationStructure?uri=${uri}`,
                method: 'get',
            })
                .then(result => {
                    if (result.status === 200) {
                        commit(
                            'setDocumentationStructure',
                            result.data.data[0],
                        );
                    }
                })
                .catch(error => {
                    console.error(error);
                });
        },
    },

    /**
     * Getters
     * Vergelijkbaar met computed in components. Bijv een filtered overzicht, of een total terug geven
     * https://vuex.vuejs.org/en/getters.html
     */
    getters: {},

    /**
     * Mutations
     * State wijzigen
     * https://vuex.vuejs.org/en/mutations.html
     */
    mutations: {
        // Detail pagina
        setDocumentationEntry(state, entry) {
            state.documentationEntry = entry;
        },

        // Menu structuur
        setDocumentationStructure(state, structure) {
            state.documentationStructure = structure;
        },
    },
});
